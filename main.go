package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/akhil/mongo-golang/controllers"
	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var session *mongo.Client

func main() {
	r := httprouter.New()

	// Initialize MongoDB session
	client, err := getSession()
	if err != nil {
		fmt.Println("Error connecting to MongoDB:", err)
		return
	}
	defer func() {
		if err := client.Disconnect(context.Background()); err != nil {
			fmt.Println("Error disconnecting from MongoDB:", err)
		}
		fmt.Println("MongoDB connection closed.")
	}()

	// Create UserController with the MongoDB session
	uc := controllers.NewUserController(client)

	// Define routes
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)

	// Create an HTTP server with custom settings
	server := &http.Server{
		Addr:    "localhost:9000",
		Handler: r,
	}

	// Start the server
	fmt.Println("Server is listening on localhost:9000...")
	err = server.ListenAndServe()
	if err != nil {
		fmt.Println("Error starting the server:", err)
	}
}

func getSession() (*mongo.Client, error) {
	// Replace "your_username" and "your_password" with actual credentials
	connectionURI := "mongodb+srv://yustikaakbar:NoAbsen36@cluster0.3io1yla.mongodb.net/users?retryWrites=true&w=majority"

	// Use the SetServerAPIOptions() method to set the version of the Stable API on the client
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	clientOptions := options.Client().ApplyURI(connectionURI).SetServerAPIOptions(serverAPI)

	// Connect to MongoDB
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		return nil, err
	}

	// Ping the MongoDB server to verify the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to MongoDB!")
	return client, nil
}
